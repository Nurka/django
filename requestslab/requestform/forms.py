from django import forms
from .models import textInput
from django.forms import ModelForm

class renderForm(forms.Form):
    input = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Enter the text'}))

class textInputForm(forms.ModelForm):
    class Meta:
        model = textInput
        fields = [
            'name'
        ]