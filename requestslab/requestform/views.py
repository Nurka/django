from django.shortcuts import render
from django.http import HttpResponse
from .forms import renderForm
from .forms import textInputForm
from .models import textInput
# Create your views here.

def index(request):
    form = renderForm()
    return render(
        request,
        'index.html',
        {'form' : form}
    )

def form(request):

    # if request.method == 'POST':
    #     form = renderForm(request.POST)
    #     if form.is_valid():
    #         text = form.cleaned_data.get('input')
    #         saveText = textInputForm(name=text)
    #         saveText.save()

    if request.method == 'POST':
            form = renderForm(request.POST)
            if form.is_valid():
                text = form.cleaned_data.get('input')

    return render(
        request,
        'form.html',
        {'text' : text}
    )