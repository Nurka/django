from django.db import models
from django.urls import reverse

# Create your models here.

class textInput(models.Model):
    name = models.CharField(max_length=200, help_text="Enter text")
    
    def __str__(self):
        return self.name
